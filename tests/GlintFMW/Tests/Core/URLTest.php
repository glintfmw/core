<?php declare (strict_types=1);
    namespace GlintFMW\Tests\Core;

    use GlintFMW\Dependencies\Injector;
    use GlintFMW\Resources\URL;
    use PHPUnit\Framework\TestCase;

    class CallableClassExample
    {
        function __invoke () {}
    }

    /**
     * URL parsing behaviour tests
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Tests\Core
     */
    class URLTest extends TestCase
    {
        /** @depends GlintFMW\Tests\Core\ClosureTest::testClosureParseFunction */
        function testURLParseFunction ()
        {
            $closure = URL::parseFunctionURL ('parse_str');

            $this->assertTrue ($closure->isFunction ());
            $this->assertFalse ($closure->isMethodFromClass ());
            $this->assertEquals ('parse_str', $closure->getMethod ());
            $this->assertNull ($closure->getClass ());
        }

        /** @depends GlintFMW\Tests\Core\ClosureTest::testClosureParseClassMethod */
        function testURLParseClassMethod ()
        {
            $closure = URL::parseFunctionURL ('inject@GlintFMW\\Dependencies\\Injector');

            $this->assertTrue ($closure->isMethodFromClass ());
            $this->assertFalse ($closure->isFunction ());
            $this->assertEquals ('inject', $closure->getMethod ());
            $this->assertEquals (Injector::class, $closure->getClass ());
        }

        function testURLParseCallableInstanceClass ()
        {
            $closure = URL::parseFunctionURL(new CallableClassExample ());

            $this->assertFalse ($closure->isFunction ());
            $this->assertFalse ($closure->isMethodFromClass ());
            $this->assertTrue ($closure->isClass ());
            $this->assertEquals (CallableClassExample::class, $closure->getClass ());
        }
    };
