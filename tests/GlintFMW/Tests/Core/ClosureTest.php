<?php declare (strict_types=1);
    namespace GlintFMW\Tests\Core;

    use GlintFMW\Types\Closure;
    use PHPUnit\Framework\TestCase;

    class CallableClass
    {
        function __invoke ()
        {
            return true;
        }
    }

    /**
     * Closure behaviour tests
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Tests\Core
     */
    class ClosureTest extends TestCase
    {
        function testClosureInexistentFunction ()
        {
            $this->expectException (\Exception::class);

            Closure::fromCallable ('inexistentMethod');
        }

        function testClosureParseFunction ()
        {
            $closure = Closure::fromCallable ('parse_str');

            $this->assertNull ($closure->getClass ());
            $this->assertEquals ('parse_str', $closure->getMethod ());
            $this->assertTrue ($closure->isFunction ());
            $this->assertFalse ($closure->isMethodFromClass ());
            $this->assertFalse ($closure->isClass ());
        }

        function testClosureParseClassMethod ()
        {
            $closure = Closure::fromCallable (array ($this, 'testClosureParse'));

            $this->assertEquals (self::class, $closure->getClass ());
            $this->assertEquals ('testClosureParse', $closure->getMethod ());
            $this->assertFalse ($closure->isFunction ());
            $this->assertTrue ($closure->isMethodFromClass ());
            $this->assertFalse ($closure->isClass ());
        }
    };