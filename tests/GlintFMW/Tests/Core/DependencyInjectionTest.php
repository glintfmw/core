<?php declare (strict_types=1);
    namespace GlintFMW\Tests\Core;

    use GlintFMW\Core\Dependencies\Exceptions\CircularDependencyException;
    use GlintFMW\Dependencies\Exceptions\ClassCannotBeInstantiated;
    use GlintFMW\Dependencies\Exceptions\DependencyNotRegisteredException;
    use GlintFMW\Dependencies\Injector;
    use PHPUnit\Framework\TestCase;

    class AutowiringDependency1
    {
        function name ()
        {
            return "dependency1";
        }
    };

    class AutowiringDependency2
    {
        function name ()
        {
            return "dependency2";
        }
    };

    class AutowiringClass
    {
        private AutowiringDependency1 $dependency1;
        private AutowiringDependency2 $dependency2;

        function __construct (AutowiringDependency1 $dependency1, AutowiringDependency2 $dependency2)
        {
            $this->dependency1 = $dependency1;
            $this->dependency2 = $dependency2;
        }

        function getDependency1 (): AutowiringDependency1
        {
            return $this->dependency1;
        }

        function getDependency2 (): AutowiringDependency2
        {
            return $this->dependency2;
        }
    };

    class CircularDependency
    {
        function __construct (CircularDependency $dependency)
        {

        }
    };

    class NotInstantiableDependency
    {
        function __construct (array $configuration, AutowiringDependency1 $dependency1)
        {

        }
    };

    class InstantiationExample
    {
        function __construct (NotInstantiableDependency $dependency)
        {

        }
    };

    class DependencyInjectionTest extends TestCase
    {
        private static Injector $injector;

        static function setUpBeforeClass (): void
        {
            self::$injector = new Injector ();
        }

        function testAutowiringDisabled ()
        {
            $this->expectException (DependencyNotRegisteredException::class);
            self::$injector->inject (AutowiringClass::class, false);
        }

        /** @depends testAutowiringDisabled */
        function testAutowiringEnabled ()
        {
            /** @var AutowiringClass $object */
            $object = self::$injector->inject (AutowiringClass::class);

            // assert autowire was done properly
            $this->assertEquals ("dependency1", $object->getDependency1 ()->name ());
            $this->assertEquals ("dependency2", $object->getDependency2 ()->name ());
        }

        /** @depends testAutowiringEnabled */
        function testResolve ()
        {
            /** @var AutowiringDependency1 $object */
            $object = self::$injector->resolve (AutowiringDependency1::class);

            // ensure the correct dependency was fetched
            $this->assertEquals ("dependency1", $object->name ());
        }

        function testCircularDependency ()
        {
            $this->expectException (CircularDependencyException::class);
            self::$injector->inject (CircularDependency::class);
        }

        /** @depends testAutowiringEnabled */
        function testNotInstantiableDependency ()
        {
            $this->expectException (ClassCannotBeInstantiated::class);
            self::$injector->inject (InstantiationExample::class);
        }
    };