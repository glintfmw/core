<?php declare (strict_types=1);
    namespace GlintFMW\Dependencies;

    use GlintFMW\Core\Dependencies\Exceptions\CircularDependencyException;
    use GlintFMW\Dependencies\Exceptions\ClassCannotBeInstantiated;
    use GlintFMW\Dependencies\Exceptions\DependencyNotRegisteredException;
    use GlintFMW\Dependencies\Exceptions\CacheNotBuiltException;

    /**
     * Dependency injector and container
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Dependencies
     */
    class Injector
    {
        const STATUS_BUILDING = 'building';
        const STATUS_DONE = 'done';

        /** @var array<string, object> Map of dependencies available */
        private array $dependencies = array ();

        /**
         * @phpstan-var array<class-string, array{constructor: array<?class-string>, status: string}>
         *
         * @var array<string, array{constructor: array<string>, status: string}> Cache for the dependency injection, shared by all the injector instances
         */
        private static array $cache = array ();

        function __construct ()
        {
            // register ourselves as dependency
            $this->registerDependency ($this);
        }

        function registerDependency (object $object): void
        {
            $this->dependencies [get_class ($object)] = $object;

            if (get_parent_class ($object) !== false)
                $this->dependencies [get_parent_class ($object)] = $object;
        }

        /**
         * Builds the construction parameters cache for the given class name
         *
         * @phpstan-param class-string $className
         * @param string $className
         *
         * @throws CacheNotBuiltException
         * @throws DependencyNotRegisteredException
         * @throws \ReflectionException
         * @throws CircularDependencyException
         */
        private static function buildCache (string $className): void
        {
            if (array_key_exists ($className, self::$cache) === true)
            {
                switch (self::$cache [$className] ['status'])
                {
                    case self::STATUS_BUILDING:
                        throw CircularDependencyException::fromCacheInfo (self::$cache);
                    case self::STATUS_DONE:
                    default:
                        return;
                }
            }

            // for classes not in cache, build it first
            self::$cache [$className] = array (
                'status' => self::STATUS_BUILDING,
                'constructor' => array ()
            );

            // the reflection object used to parse the parameters
            $reflection = new \ReflectionClass ($className);

            $constructor = $reflection->getConstructor ();

            if (is_null ($constructor) === true)
                $dependencies = array ();
            else
                $dependencies = $constructor->getParameters ();

            foreach ($dependencies as $dependency)
            {
                if (is_null ($dependency->getClass ()) === false)
                {
                    $dependencyClassName = $dependency->getClass ()->getName ();
                    // ensure there is cache for this class
                    self::buildCache ($dependencyClassName);
                    // finally save the required constructor parameters
                    self::$cache [$className] ['constructor'] [] = $dependencyClassName;
                }
                else
                {
                    self::$cache [$className] ['constructor'] [] = null;
                }
            }

            // mark cache building as done for this class
            self::$cache [$className] ['status'] = self::STATUS_DONE;
        }

        /**
         * Performs the actual class creation with the injected parameters
         *
         * @param string $className The class to instantiate
         * @param bool $autowiring Indicates if the dependencies can be created automatically or not
         * @param bool $injectable Indicates if the created instance can be injected into others
         *
         * @return object The instantiated class with the correct dependencies injected
         *
         * @throws CacheNotBuiltException
         * @throws CircularDependencyException
         * @throws ClassCannotBeInstantiated
         * @throws DependencyNotRegisteredException
         * @throws \ReflectionException
         */
        private function injectFromCache (string $className, bool $autowiring, bool $injectable): object
        {
            if (array_key_exists ($className, self::$cache) === false)
            {
                throw new CacheNotBuiltException ($className);
            }

            $dependencies = array ();

            foreach (self::$cache [$className] ['constructor'] as $order => $dependencyClassName)
            {
                if (is_null ($dependencyClassName) === true)
                {
                    throw new ClassCannotBeInstantiated ($className);
                }
                // inject non-registered classes only when autowiring is requested
                elseif (array_key_exists ($dependencyClassName, $this->dependencies) === false)
                {
                    if ($autowiring === false)
                        throw new DependencyNotRegisteredException ($dependencyClassName);
                    else
                        $this->inject ($dependencyClassName, true);
                }

                $dependencies [$order] = $this->dependencies [$dependencyClassName];
            }

            $object = new $className (...$dependencies);

            // register the recently injected object as a possible dependency
            if ($injectable)
                $this->registerDependency ($object);

            return $object;
        }

        /**
         * Creates a class injecting the required services trough the constructor
         *
         * @phpstan-param class-string $className
         * @param string $className The class to instantiate
         * @param bool $autowiring Indicates if the dependencies can be created automatically or not
         * @param bool $injectable Indicates if the created instance can be injected into others
         *
         * @return object The instantiated class
         *
         * @throws CacheNotBuiltException Will never be thrown on normal flow
         * @throws DependencyNotRegisteredException If the dependency requested doesn't exist and cannot be automatically created
         * @throws \ReflectionException If the reflection of the constructor failed
         * @throws CircularDependencyException If there is any circular dependency detected
         */
        function inject (string $className, bool $autowiring = true, bool $injectable = true): object
        {
            if (array_key_exists ($className, self::$cache) === false)
                self::buildCache ($className);

            return $this->injectFromCache ($className, $autowiring, $injectable);
        }

        /**
         * Looks up the injection list, resolves the dependency name and returns the object
         *
         * @param string $className Class to resolve
         *
         * @return object The resolved class
         * @throws DependencyNotRegisteredException
         */
        function resolve (string $className): object
        {
            $className = ltrim ($className, "\\");

            if (array_key_exists ($className, $this->dependencies) === true)
                return $this->dependencies [$className];

            $parentClassName = get_parent_class ($className);

            // try auto-wiring if the class isn't registered
            if ($parentClassName === false || array_key_exists ($parentClassName, $this->dependencies) === false)
                throw new DependencyNotRegisteredException ($className);

            return $this->dependencies [$parentClassName];
        }
    };