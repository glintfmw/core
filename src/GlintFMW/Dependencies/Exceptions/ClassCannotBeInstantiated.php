<?php declare (strict_types=1);
    namespace GlintFMW\Dependencies\Exceptions;

    class ClassCannotBeInstantiated extends \Exception {};