<?php declare (strict_types=1);
    namespace GlintFMW\Dependencies\Exceptions;

    /**
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Dependencies\Exceptions
     */
    class DependencyNotRegisteredException extends \Exception {};