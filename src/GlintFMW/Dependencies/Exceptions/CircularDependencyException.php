<?php declare (strict_types=1);
    namespace GlintFMW\Core\Dependencies\Exceptions;

    use GlintFMW\Dependencies\Injector;

    class CircularDependencyException extends \Exception
    {
        /**
         * Builds a exception based on the given cache snapshot
         *
         * @phpstan-param array<class-string, array{constructor: array<?class-string>, status: string}> $cache
         * @param array $cache
         * @return CircularDependencyException
         */
        public static function fromCacheInfo (array $cache)
        {
            $message = '';

            foreach ($cache as $className => $data)
            {
                if ($data ['status'] == Injector::STATUS_BUILDING)
                    $message .= $className . PHP_EOL;

            }

            return new self ("Detected circular dependency injection: {$message}");
        }
    };