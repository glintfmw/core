<?php declare (strict_types=1);
    namespace GlintFMW\Types\Exceptions;

    /**
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Types\Exceptions
     */
    class ClassTypeDoesNotMatchException extends \Exception {};