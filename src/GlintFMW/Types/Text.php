<?php declare(strict_types=1);
	namespace GlintFMW\Types;
	
	/**
	 * A simple class for string parsing
	 *
	 * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Types
	 */
	class Text
	{
		/** Indicates the search should be done on the begining only */
		const SEARCH_BEGINNING = 0;
		/** Indicates the search should be done anywhere on the string */
		const SEARCH_ANY = 1;
		/** Indicates the search should be done from the current point */
		const SEARCH_CONTINUE = 2;
		/** Indicates the search should be match inmmediately from the current point */
		const SEARCH_INMEDIATE = 3;
		/** Indicates the search should match the next word from the current point */
		const SEARCH_NEXT = 4;
		
		/**
		 * @var string The message text
		 */
		private string $text = "";
		
		/**
		 * @var int The point we're reading from in the string
		 */
		private $pointer = 0;
		
		/**
		 * @param string $text The text sent in the message
		 */
		function __construct ($text)
		{
			$this->text = $text;
		}
		
		/**
		 * Checks if the message contains the specified information and updates the pointer if requested
		 *
		 * @param array<string>|string $input The criteria to search
		 * @param int $position Where to search. Possible values {self::SEARCH_ANY}, {self::SEARCH_BEGINNING}, {self::SEARCH_CONTINUE}, {self::SEARCH_INMEDIATE} and {self::SEARCH_NEXT}
		 * @param bool $updatePointer Wheter the position pointer of the string should be updated or not
		 *
		 * @return bool Indicates if the $input exists or not
		 */
		private function find ($input, $position, $updatePointer)
		{
			if (is_array ($input) == false)
			{
				$input = array ($input);
			}
			
			foreach ($input as $criteria)
			{
				$pointerTmp = 0;
				$return = false;
				
				switch ($position)
				{
					case self::SEARCH_ANY:
						$pointerTmp = stripos ($this->text, $criteria);
						$return = $pointerTmp !== false;
						break;
					
					case self::SEARCH_BEGINNING:
						$pointerTmp = stripos ($this->text, $criteria);
						$return = $pointerTmp === 0;
						break;
					
					case self::SEARCH_CONTINUE:
						$pointerTmp = stripos ($this->text, $criteria, $this->pointer);
						$return = $pointerTmp !== false;
						break;
					
					case self::SEARCH_INMEDIATE:
						$pointerTmp = stripos ($this->text, $criteria, $this->pointer);
						$return = $pointerTmp === $this->pointer;
						break;
					
					case self::SEARCH_NEXT:
					{
						$space = 0;
						
						// searching for a white space is only needed when we're not at the beginning of the message
						if ($this->pointer > 0)
						{
							$space = stripos ($this->text, " ", $this->pointer);
							
							if ($space === false)
							{
								$space = stripos ($this->text, "-", $this->pointer);
							}
							
							if ($space === false)
							{
								$space = stripos ($this->text, ",", $this->pointer);
							}
							
							// at the end of the message, use current point
							if ($space === false)
							{
								$space = $this->pointer;
							}
						}

                        $pointerTmp = stripos ($this->text, $criteria, $space);
                        $return = $pointerTmp !== false && $pointerTmp <= ($space + 3);
					}
						break;
				}
				
				if ($return == true)
				{
					if ($updatePointer == true && $pointerTmp !== false)
					{
						$this->setPointer ($pointerTmp + strlen ($criteria));
					}
					
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Check if the message contains the specified information. This function DOES NOT
		 * advance the string pointer forward
		 *
		 * @param array<string>|string $input The criteria to search
		 * @param int $position Where to search. Possible values {self::SEARCH_ANY}, {self::SEARCH_BEGINNING}, {self::SEARCH_CONTINUE}, {self::SEARCH_INMEDIATE} and {self::SEARCH_NEXT}
		 *
		 * @return bool Indicates if the input exists or not
		 */
		function contains ($input, $position = self::SEARCH_CONTINUE)
		{
			return $this->find ($input, $position, false);
		}
		
		/**
		 * Check if the message contains the specified information. This function SETS the pointer forward
		 *
		 * @param array<string>|string $input The criteria to search
		 * @param int $position Where to search. Possible values {self::SEARCH_ANY}, {self::SEARCH_BEGINNING}, {self::SEARCH_CONTINUE}, {self::SEARCH_INMEDIATE} and {self::SEARCH_NEXT}
		 *
		 * @return bool Indicates if the input was found or not
		 */
		function has ($input, $position = self::SEARCH_CONTINUE)
		{
			return $this->find ($input, $position, true);
		}
		
		/**
		 * Resets the message pointer to the begining of the message
		 *
		 * @return $this
		 */
		function resetPointer (): self
		{
			$this->pointer = 0;
			
			return $this;
		}
		
		/**
		 * @return int The current pointer position
		 */
		function getPointer (): int
		{
			return $this->pointer;
		}
		
		/**
		 * Updates the string pointer to continue to search from when self::SEARCH_CONTINUE is specified
		 *
		 * @param int $pointer The new position for the string pointer
		 *
		 * @return $this
		 */
		function setPointer (int $pointer): self
		{
			$this->pointer = $pointer;
			
			return $this;
		}
		
		/**
		 * @return string The message text
		 */
		function getText (): string
		{
			return $this->text;
		}
	};