<?php declare (strict_types=1);
    namespace GlintFMW\Types;

    use GlintFMW\Types\Exceptions\ClassTypeDoesNotMatchException;

    /**
     * Class representation of a callable
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Types
     */
    class Closure
    {
        /** @var int The callback is a method in a class */
        const TYPE_METHOD = 0;
        /** @var int The callback is a sole function */
        const TYPE_FUNCTION = 1;
        /** @var int The callback is a class with __invoke or __invokeStatic */
        const TYPE_CLASS = 2;

        /**
         * @phpstan-var class-string|null
         * @var string|null The fully qualified class name
         */
        private ?string $class = null;
        /** @var string|null The method/function name */
        private ?string $method = null;

        /**
         * @phpstan-return class-string|null
         * @return string|null The class name (if any)
         */
        function getClass (): ?string
        {
            return $this->class;
        }

        /**
         * @phpstan-param class-string|null $class
         *
         * @param string|null $class The new class name
         * @return $this
         */
        function setClass (?string $class): self
        {
            $this->class = $class;

            return $this;
        }

        /**
         * @return string|null The method to call
         */
        function getMethod (): ?string
        {
            return $this->method;
        }

        /**
         * @param string|null $method The new method to call
         * @return $this
         */
        function setMethod (?string $method): self
        {
            $this->method = $method;

            return $this;
        }

        /**
         * @return int The type of callable this callback represents
         */
        private function getType (): int
        {
            if (isset ($this->class) == false || is_null ($this->class) == true)
                return self::TYPE_FUNCTION;
            if (isset ($this->method) == false || is_null ($this->method) == true)
                return self::TYPE_CLASS;
            return self::TYPE_METHOD;
        }

        /**
         * @return bool Indicates if the Callback is a class's method callback
         */
        public function isMethodFromClass (): bool
        {
            return $this->getType () == self::TYPE_METHOD;
        }

        /**
         * @return bool Indicates if the callback is a function callback
         */
        public function isFunction (): bool
        {
            return $this->getType () == self::TYPE_FUNCTION;
        }

        /** @return bool Indicates if the callback is a callable class */
        public function isClass (): bool
        {
            return $this->getType () == self::TYPE_CLASS;
        }

        /**
         * Generates a \Closure to be used on others part of the code
         *
         * @param object|null $object The object to generate the callable for (if any)
         * @return \Closure The callable
         * @throws ClassTypeDoesNotMatchException
         */
        public function getClosure (?object $object = null): \Closure
        {
            return \Closure::fromCallable (
                $this->getCallable ($object)
            );
        }

        /**
         * Obtains a reflection object for this closure
         *
         * @return \ReflectionFunctionAbstract The reflection object
         * @throws \ReflectionException If the reflection failed
         */
        public function getReflection (): \ReflectionFunctionAbstract
        {
            switch ($this->getType ())
            {
                case self::TYPE_FUNCTION:
                    // @phpstan-ignore-next-line
                    return new \ReflectionMethod ($this->getMethod ());
                case self::TYPE_METHOD:
                    // @phpstan-ignore-next-line
                    return new \ReflectionMethod ($this->getClass (), $this->getMethod ());
                case self::TYPE_CLASS:
                    // @phpstan-ignore-next-line
                    return new \ReflectionMethod ($this->getClass (), '__invoke');
                default:
                    throw new \ReflectionException ("Cannot determine method to reflect");
            }
        }

        /**
         * Creates a new Callback from a callable
         *
         * @param array{class-string|object,string}|string|object $data
         * @return self
         */
        public static function fromCallable ($data): self
        {
            $callable = new self ();

            if (is_string ($data) === true)
            {
                $colon = strpos ($data, '::');

                // check for double-colons first
                if ($colon === false)
                {
                    // normal classes
                    if (class_exists ($data) === true)
                        $callable->setClass ($data);
                    // normal functions
                    elseif (function_exists ($data) === true)
                        $callable->setMethod ($data);
                    else
                        throw new \Exception ("The given callable doesn't exist");
                }
                else
                {
                    /** @var class-string|false */
                    $class = substr ($data, 0, $colon);
                    /** @var string|false */
                    $method = substr ($data, $colon + strlen ('::'));

                    if ($class === false || $method === false)
                        throw new \Exception ("Cannot parse class and/or method name on Class::Method string");

                    $callable
                        ->setClass ($class)
                        ->setMethod ($method);
                }
            }
            // php's own callables
            elseif (is_array ($data) === true)
            {
                $classname = "";

                if (count ($data) != 2)
                    throw new \Exception ("The given callable (array) doesn't have enough elements");

                if (is_string ($data [0]) == false)
                    $classname = get_class ($data [0]);
                else
                    $classname = $data [0];

                // @phpstan-ignore-next-line
                if (is_string ($classname) == false)
                    throw new \Exception ("Cannot determine class name for array callable");

                // @phpstan-ignore-next-line
                if (is_string ($data [1]) == false)
                    throw new \Exception ("Unexpected method type, must be string");

                $callable
                    ->setClass ($classname)
                    ->setMethod ($data [1]);
            }
            elseif (is_object ($data) === true)
            {
                // normal classes
                if (is_callable ($data) === true)
                    $callable->setClass (get_class ($data));
                else
                    throw new \Exception ("The given object is not callable");
            }

            return $callable;
        }

        public function getCallable (?object $object): callable
        {
            /** @var ?callable $callable */
            $callable = null;

            if ($this->isFunction () == true)
            {
                $callable = $this->getMethod ();
            }
            elseif ($this->isClass () == true)
            {
                if (is_null ($object) === true)
                    throw new \Exception ("Callable classes must be specified an instance to perform the call on");
                if ($this->getClass () !== get_class ($object))
                    throw new ClassTypeDoesNotMatchException ();

                $callable = $object;
            }
            else
            {
                $class = '';

                if (is_null ($object) === false)
                {
                    if ($this->getClass () !== get_class ($object))
                    {
                        throw new ClassTypeDoesNotMatchException ();
                    }

                    $class = $object;
                }
                else
                {
                    $class = $this->getClass ();
                }

                $callable = array ($class, $this->getMethod ());
            }

            if (is_callable ($callable) === false)
                throw new \Exception ("Expected callable generation");

            return $callable;
        }

        /**
         * @param array{class:class-string,method:string} $input
         * @return self
         */
        public static function __set_state (array $input): self
        {
           $callback = new self ();

           if (array_key_exists ('class', $input) == true)
               $callback->setClass ($input ['class']);
           if (array_key_exists ('method', $input) == true)
               $callback->setMethod ($input ['method']);

           return $callback;
        }

        /**
         *
         * @param object|null $object
         * @param array<string, mixed> $parameters
         *
         * @return mixed Whatever the call returned
         *
         * @throws ClassTypeDoesNotMatchException
         * @throws \ReflectionException
         */
        function call (?object $object, array $parameters)
        {
            $finalParameters = array ();
            $reflection = $this->getReflection ();

            // prepare the parameters in the order they are assigned in the method
            // this way we don't have to care about the order they're from whatever they're being generated
            foreach ($reflection->getParameters () as $parameter)
            {
                if (array_key_exists ($parameter->getName (), $parameters) === false)
                {
                    if ($parameter->isDefaultValueAvailable () == false)
                        throw new \InvalidArgumentException ("Call expects parameter {$parameter->getName ()}");

                    // use default values
                    $finalParameters [] = $parameter->getDefaultValue ();
                }
                else
                {
                    $finalParameters [] = $parameters [$parameter->getName ()];
                }
            }

            return call_user_func_array ($this->getCallable ($object), $finalParameters);
        }

        function __toString (): string
        {
            switch ($this->getType ())
            {
                case self::TYPE_METHOD:
                    return $this->getClass () . "@" . $this->getMethod ();
                case self::TYPE_CLASS:
                    return $this->getClass () ?? '';
                case self::TYPE_FUNCTION:
                    return $this->getMethod () ?? '';
                default:
                    throw new \Exception ('Cannot determine closure type to obtain it\'s string representation');
            }
        }
    };