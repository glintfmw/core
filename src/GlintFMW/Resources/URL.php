<?php declare (strict_types=1);
    namespace GlintFMW\Resources;

    use GlintFMW\Types\Closure;

    /**
     * Resource URL utilities for the Framework
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Resources
     */
    class URL
    {
        /** @var string The separator to search for in the class/function string */
        const CLASS_FUNCTION_SEPARATOR = '@';

        /**
         * @param string|object $url
         * @return \GlintFMW\Types\Closure
         * @throws \Exception
         */
        public static function parseFunctionURL ($url): \GlintFMW\Types\Closure
        {
            $callable = array ();

            if (is_object ($url) == true)
            {
                $callable = $url;
            }
            else
            {
                $parts = explode (self::CLASS_FUNCTION_SEPARATOR, $url);

                if (count ($parts) === 1)
                {
                    if (function_exists ($parts [0]) === false && class_exists ($parts [0]) === false)
                        throw new \Exception ("The specified callable doesn't exist");

                    $callable = $parts [0];
                }
                elseif (count ($parts) === 2)
                {
                    if (class_exists ($parts [1]) === false)
                        throw new \Exception ("The class {$parts [1]} doesn't exist");
                    if (method_exists ($parts [1], $parts [0]) === false)
                        throw new \Exception ("The method {$parts [0]} doesn't exist in class {$parts [1]}");

                    $callable = array ($parts [1], $parts [0]);
                }
                else
                {
                    throw new \Exception ('The function URI is not valid');
                }
            }

            // ensure the callable is one of the two formats
            if (is_string ($callable) === true && function_exists ($callable) === false)
            {
                if (class_exists ($callable) === false)
                throw new \Exception ('The function URI is not valid');
            }
            elseif (is_callable ($callable) == false)
                throw new \Exception ('The function URI is not valid');


            return Closure::fromCallable ($callable);
        }
    };