<?php declare (strict_types=1);
    namespace GlintFMW;

    use GlintFMW\Dependencies\Injector;

    /**
     * Base class for applications based on the GlintFMW framework
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW
     */
    abstract class Application
    {
        protected Injector $injector;

        /**
         * Constructor for the application
         *
         * @param array<string, mixed> $configuration   The configuration used for the application
         *                                              this configuration is fed to all the configuration providers
         *                                              specified in it
         */
        final function __construct (array $configuration = array ())
        {
            $this->injector = new Injector ();

            foreach ($configuration as $priority => $classList)
            {
                foreach ($classList as $name => $config)
                {
                    $this->injector->registerDependency (
                        new $name ($config, $this->injector)
                    );
                }
            }
        }

        /**
         * @return Injector The injector used by the app for dependencies loading
         */
        public function getInjector (): Injector
        {
            return $this->injector;
        }

        /**
         * Main application body
         */
        abstract function run (): void;
    };